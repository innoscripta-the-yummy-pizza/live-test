import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Components import
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Sidebar from './components/Sidebar/Sidebar';

// Pages import (this has to be a routing)
import Home from './pages/Home/Home';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.toggleClass= this.toggleClass.bind(this);
    this.state = {
      active: false,
    };
  }

  toggleClass() {
    const currentState = this.state.active;
    this.setState({ active: !currentState });
  };

  render() {
    const { active: currentState } = this.state;
    return (
      <div className={`App d-flex ${currentState ? 'toggled': null}`} id="wrapper">
        <Sidebar />
          <div id="page-content-wrapper">
            <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom">
              <button className="btn btn-primary" id="menu-toggle" onClick={this.toggleClass} >Toggle Menu</button>
            </nav>
  
            <div className="container-fluid">
              <br />
              <Header />
              <Home />
              <Footer />
            </div>
          </div>
      </div>
    );
  }
}

export default App;
