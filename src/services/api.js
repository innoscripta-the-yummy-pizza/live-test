import axios from 'axios';

/**
 * Github GET request
 * curl -G https://api.github.com/search/repositories --data-urlencode "sort=stars" --data-urlencode "order=desc" --data-urlencode "q=language:java"  --data-urlencode "q=created:>`date -v-7d '+%Y-%m-%d'`" | jq ".items[0,1,2] | {name, description, language, watchers_count, html_url}"
 */
export default async function getTrending (query) {
  let url = 'https://api.github.com/search/repositories?sort=stars&order=desc&q=language:java';
  if (query) {
    url += query;
  }
  return axios.get(url);
}