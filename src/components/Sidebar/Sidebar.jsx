import React from 'react';

function Sidebar() {
  return (
    <div className="bg-light border-right" id="sidebar-wrapper">
      <div className="sidebar-heading">Innoscripta</div>
      <div className="list-group list-group-flush">
        <a href="https://google.com" className="list-group-item list-group-item-action bg-light">Dashboard</a>
        <a href="https://google.com" className="list-group-item list-group-item-action bg-light">Overview</a>
        <a href="https://google.com" className="list-group-item list-group-item-action bg-light">Events</a>
        <a href="https://google.com" className="list-group-item list-group-item-action bg-light">Profile</a>
        <a href="https://google.com" className="list-group-item list-group-item-action bg-light">Status</a>
      </div>
    </div>
  );
}

export default Sidebar;
