import React from 'react';
import Card from 'react-bootstrap/Card';
import Spinner from 'react-bootstrap/Spinner';

import getTrending from '../../services/api';

class Home extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      isLoading: true,
      value: '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState({ value });

    setTimeout(() => {
      const { value: oldValue } = this.state;
      if (oldValue === value) {
        // Clear all timeouts first
        let id = window.setTimeout(() => {}, 0);
        while (id--) {
          window.clearTimeout(id);
        }

        // Perform the filtering
        const query = `&q=${encodeURIComponent(value)}:in:name&q=${encodeURIComponent(value)}:in:full_name`;
        this.setState({ results: [], isLoading: true });
        getTrending(query).then(response => {
          const results = response.data.items;
          this.setState({ results, isLoading: false });
        });
      }
    }, 2000);
  }

  componentDidMount() {
    getTrending().then(response => {
      const results = response.data.items;
      this.setState({ results, isLoading: false });
    });
  }

  render() {
    const {
      results: repositories,
      isLoading,
    } = this.state;
    return (
      <div className="page-content">
        <div className="container">
          <div className="row">
            <form>
              <label>
                Search:&nbsp;&nbsp;
                <input type="text" onChange={this.handleChange} onKeyUp={this.handleChange} />
              </label>
            </form>
          </div>
          <div className="row">
            {isLoading ? <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner> : null}
          </div>
          <div className="row">
            {repositories.map((repository, index) => (
              <Card key={index}>
                <Card.Header>
                  <a href={repository.html_url} target="_blank" rel="noopener noreferrer">{repository.full_name}</a>
                </Card.Header>
                <Card.Body>
                  <Card.Title>{repository.name}</Card.Title>
                  <Card.Text>{repository.description}</Card.Text>
                  <div className="row">
                    <div className="col">
                      <p><b>Language: </b>{repository.language}</p>
                    </div>
                    <div className="col">
                      <p><b>Score: </b>{repository.score}</p>
                    </div>
                    <div className="col">
                      <p><b>Forks: </b>{repository.forks}</p>
                    </div>
                  </div>
                </Card.Body>
              </Card>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Home;