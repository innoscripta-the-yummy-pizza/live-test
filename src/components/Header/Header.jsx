import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';

function Header() {
  return (
    <header className="main-header">
      <Jumbotron fluid>
        <Container>
          <h1>Responsive WebApp</h1>
          <p>Responsive web app live code. Innoscripta.</p>
        </Container>
      </Jumbotron>
    </header>
  );
}

export default Header;
