import React from 'react';

function Footer() {
  return (
    <footer className="main-footer">
      <div className="container">
        <div className="row">
          <div className="col-6 col-md-3">
            <ul>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
            </ul>
          </div>
          <div className="col-6 col-md-3">
            <ul>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
            </ul>
          </div>
          <div className="col-6 col-md-3">
            <ul>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
            </ul>
          </div>
          <div className="col-6 col-md-3">
            <ul>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
              <li><a href="https://google.com">Link</a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
